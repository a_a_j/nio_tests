package service;

import dao.StudentDao;
import model.Group;
import model.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;
import static org.mockito.Matchers.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {
    @Mock private FileService fileService;
    @Mock private Student student;
    @Mock private Group group;
    @Mock private StudentDao studentDao;
    @Mock private FactoryHelper factoryHelper;
    //@Spy FactoryHelper factoryHelper = new FactoryHelper();

    @InjectMocks
    private StudentService studentService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void getStudentByIdDaoReturnsNull() {
        when(studentDao.getStudent(anyInt())).thenReturn(eq(null));
        assertNull(studentService.getStudentById(1));
    }

    @Test
    public void getStudentByIdDaoReturnsStudent() {
        student = new Student(1, null, null, null);
        when(studentDao.getStudent(anyInt())).thenReturn(student);
        assertNotNull(studentService.getStudentById(1));
    }

    @Test
    public void saveServiceWithNullArg() {
        boolean result;
        result = studentService.save(null);
        assertFalse(result);
    }

    @Test
    public void saveWithArg() {
        boolean result;
        result = studentService.save(student);
        assertFalse(result);
    }

    @Test
    public void saveStudentGetIdReturnsNullStudent() {
        boolean result;
        when(student.getId()).thenReturn(1);
        result = studentService.save(student);
        assertFalse(result);
    }

    @Test
    public void saveStudentGetGroupReturnsNull() {
        boolean result;
        when(student.getId()).thenReturn(1);
        when(student.getGroup()).thenReturn(null);
        result = studentService.save(student);
        assertFalse(result);
    }

    @Test
    public void saveSaveToFileReturnsFalse() {
        boolean result;
        when(student.getId()).thenReturn(1);
        when(student.getGroup()).thenReturn(new Group(1, "test", 1990));
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.objToJson(any(Student.class))).thenReturn("Content");
        when(fileService.saveToFile(anyString(), anyString())).thenReturn(false);
        result = studentService.save(student);
        assertFalse(result);
    }

    @Test
    public void getStudentServiceGetReturnsNull() {
        Student student;
        student = studentService.get(null);
        assertNull(student);
    }

    @Test
    public void getGetExactFilePathReturnsNull() {
        Student student;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.getExactFilePath(anyString(), anyString())).thenReturn(null);
        student = studentService.get(1);
        assertNull(student);
    }

    @Test
    public void getLoadFromFileReturnsNull() {
        Student student;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.getExactFilePath(anyString(), anyString())).thenReturn("path");
        when(fileService.loadFromFile(anyString())).thenReturn(null);
        student = studentService.get(1);
        assertNull(student);
    }

    @Test
    public void getJsonToObjReturnsNull() {
        Student student;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.getExactFilePath(anyString(), anyString())).thenReturn("path");
        when(fileService.loadFromFile(anyString())).thenReturn("file");
        Student realStudent = new Student(1, null, null, null);
        when(fileService.jsonToObj(anyString(), eq(realStudent.getClass()))).thenReturn(null);
        student = studentService.get(1);
        assertNull(student);
    }

    @Test
    public void getStudentsGetExactFilePathReturnsNull() {
        List<Student> studentList;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.getExactFilePath(anyString(), anyString())).thenReturn(null);
        studentList = studentService.getStudents(1);
        assertEquals(0, studentList.size());
    }

    @Test
    public void getStudentsGetFileNamesFromDirReturnsEmptyList() {
        List<Student> studentList;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.getFileNamesFromDir(anyString())).thenReturn(new ArrayList<>());
        studentList = studentService.getStudents(1);
        assertEquals(0, studentList.size());
    }

    @Test
    public void getStudentsLoadFromFileReturnsNull() {
        List<Student> studentList;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.loadFromFile(anyString())).thenReturn(null);
        studentList = studentService.getStudents(1);
        assertEquals(0, studentList.size());
    }

    @Test
    public void getStudentsJsonToObjRetunsNull() {
        List<Student> studentList;
        when(factoryHelper.makeFileService()).thenReturn(fileService);
        when(fileService.jsonToObj(anyString(), eq(Student.class))).thenReturn(null);
        studentList = studentService.getStudents(1);
        assertEquals(0, studentList.size());
    }
}
