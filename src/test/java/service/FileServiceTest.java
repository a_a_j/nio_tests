package service;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static junit.framework.TestCase.*;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.NonWritableChannelException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.mockito.Mock;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
Этот класс я не смог протестить :(
**/

//@RunWith(MockitoJUnitRunner.class)
public class FileServiceTest {
    @Mock private FactoryHelper factoryHelper;
    @Mock private FileChannel fileChannel;
    @Mock private RandomAccessFile randomAccessFile;

    @InjectMocks
    private FileService fileService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void saveToFileNullArgs() {
        boolean result = fileService.saveToFile(null, null);
        assertFalse(result);
    }

    /*@Test
    public void saveToFileChannelWriteError() throws IOException {
        when(factoryHelper.makeRandomAccessFile(anyString(), anyString())).thenReturn(randomAccessFile);
        when(randomAccessFile.getChannel()).thenReturn(fileChannel);
        //when(fileChannel.write((ByteBuffer) any())).thenThrow(NonWritableChannelException.class);
        boolean result = fileService.saveToFile("data/test.txt", "test");
        assertFalse(result);
    }*/

    @Test
    public void objToJsonMakeGsonReturnsNull() {
        when(factoryHelper.makeGson()).thenReturn(null);
        String result = fileService.objToJson(new Object());
        assertNull(result);
    }

    /*@Test
    public void objToJsonToJsonReturnsNull() {
        when(factoryHelper.makeGson()).thenReturn(gson);
        when(gson.toJson(any(Object.class))).thenReturn(null);
        String result = fileService.objToJson(new Object());
        assertNull(result);
    }*/

}
