package service;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

public class FactoryHelper {

    private static FactoryHelper factoryHelper;

    private FactoryHelper() {
    }

    public static FactoryHelper getInstance() {
        if (factoryHelper == null) {
            factoryHelper = new FactoryHelper();
        }
        return factoryHelper;
    }

    FileService makeFileService() {
        return new FileService();
    }

    Gson makeGson() {
        return new Gson();
    }

    RandomAccessFile makeRandomAccessFile(String path, String mode) throws FileNotFoundException {
        return new RandomAccessFile(path, mode);
    }
}
