package service;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

// класс для работы с файлами и директориями
public class FileService {
    private FactoryHelper factoryHelper;

    public FileService() {
        this.factoryHelper = FactoryHelper.getInstance();
    }

    // сохранение файла на диск
    public Boolean saveToFile(String filePath, String content) {
        boolean result = false;
        if (filePath != null && content != null) {
            try {
                Path dir = Paths.get(filePath).getParent();
                prepareDirectory(dir);
                RandomAccessFile aFile = factoryHelper.makeRandomAccessFile(filePath, "rw");
                FileChannel fileChannel = aFile.getChannel();
                ByteBuffer byteBuffer = ByteBuffer.allocate(512);
                byteBuffer.clear();
                byteBuffer.put(content.getBytes());
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()) {
                    fileChannel.write(byteBuffer);
                }
                aFile.close();
                result = true;
            } catch (IOException e) {
                result = false;
            }
        }
        return result;
    }

    // загрузка файла с диска, возвращает содержимое в String
    public String loadFromFile(String filePath) {
        String fileString = null;
        try {
            RandomAccessFile aFile = factoryHelper.makeRandomAccessFile(filePath, "rw");
            FileChannel fromChannel = aFile.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            int bytesRead = 0;
            StringBuilder tmp = new StringBuilder();
            while (bytesRead != -1) {
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()) {
                    tmp.append((char) byteBuffer.get());
                }
                byteBuffer.clear();
                bytesRead = fromChannel.read(byteBuffer);
            }
            fileString = tmp.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileString;
    }

    // создает путь, если таковой отсутствует
    public void prepareDirectory(Path dir) {
        if (Files.notExists(dir)) {
            try {
                Files.createDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // возвращает точное нахождение файла в глубине директории
    public String getExactFilePath(String rootDir, String fileName) {
        String filePath = null;
        List<Path> files = new ArrayList<>();
        Path path = Paths.get(rootDir);
        try {
            Files.walk(path).forEach(files::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Path p : files) {
            if (p.getFileName().toString().toLowerCase().equals(fileName.toLowerCase())) {
                filePath = p.toString();
                break;
            }
        }
        return filePath;
    }

    // возвращает перечень файлов первого уровня в директории (кроме папок)
    public List<String> getFileNamesFromDir(String dir) {
        List<String> fileNames = new ArrayList<>();
        DirectoryStream<Path> directoryStream;
        try {
            directoryStream = Files.newDirectoryStream(Paths.get(dir));
            for (Path path : directoryStream) {
                if (Files.isRegularFile(path)) {
                    fileNames.add(path.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileNames;
    }

    // сериализация объект в json
    public String objToJson(Object obj) {
        String json = null;
        Gson gson = factoryHelper.makeGson();
        if (gson != null) {
            json = gson.toJson(obj);
        }
        return json;
    }

    // десериализация json в объект
    public Object jsonToObj(String objString, Class clazz) {
        Object object;
        Gson gson = factoryHelper.makeGson();
        object = gson.fromJson(objString, clazz);
        return object;
    }

}
