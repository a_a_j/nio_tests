package service;

import com.google.gson.Gson;
import dao.StudentDao;
import model.Group;
import model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentService {
    private StudentDao studentDao;
    private FactoryHelper factoryHelper;

    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
        this.factoryHelper = FactoryHelper.getInstance();
    }

    public Student getStudentById (int id) {
        return studentDao.getStudent(id);
    }

    public Boolean save(Student student) {
        boolean result = false;
        if (student != null) {
            Integer id = student.getId();
            if (id != null && id > 0) {
                Group group = student.getGroup();
                if (group != null) {
                    Integer idGroup = group.getId();
                    if (idGroup != null && idGroup > 0) {
                        String filePath = "data/group-" + idGroup + "/student-" + id + ".sv";
                        FileService fileService = factoryHelper.makeFileService();
                        String jsonString = fileService.objToJson(student);
                        result = fileService.saveToFile(filePath, jsonString);
                    }
                }
            }
        }
        return result;
    }

    public Student get(Integer id) {
        Student student = null;
        if (id != null && id > 0) {
            FileService fileService = factoryHelper.makeFileService();
            // getExactFilePath возвращает точное нахождение файла в глубине директории
            String filePath = fileService.getExactFilePath("data", "student-"+id+".sv");
            if (filePath != null) {
                String objString = fileService.loadFromFile(filePath);
                if (objString != null) {
                    Object obj = fileService.jsonToObj(objString, Student.class);
                    if (obj != null) {
                        student = (Student) obj;
                    }
                }
            }
        }
        return student;
    }

    public List<Student> getStudents(Integer groupId) {
        List<Student> studentList = new ArrayList<>();
        if (groupId != null) {
            FileService fileService = factoryHelper.makeFileService();
            String groupFolder = "group-" + groupId;
            String groupDir = fileService.getExactFilePath("data", groupFolder);
            if (groupDir != null) {
                List<String> fileNames = fileService.getFileNamesFromDir(groupDir);
                if (fileNames != null) {
                    for (String fName : fileNames) {
                        String objString = fileService.loadFromFile(fName);
                        if (objString != null) {
                            Student student = (Student) fileService.jsonToObj(objString, Student.class);
                            if (student != null) {
                                studentList.add(student);
                            }
                        }
                    }
                }
            }
        }
        return studentList;
    }
}
