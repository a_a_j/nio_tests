package model;

public class Group {
    Integer id;
    String name;
    Integer graduationYear;

    public Group(Integer id, String name, Integer graduationYear) {
        this.id = id;
        this.name = name;
        this.graduationYear = graduationYear;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(Integer graduationYear) {
        this.graduationYear = graduationYear;
    }
}
