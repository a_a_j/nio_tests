package model;

public class Student {
    Integer id;
    String fio;
    String city;
    Group group;

    public Student(Integer id, String fio, String city, Group group) {
        this.id = id;
        this.fio = fio;
        this.city = city;
        this.group = group;
    }

    public Integer getId() {
        if (id != null) {
            return id;
        } else {
            return 0;
        }
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", city='" + city + '\'' +
                ", group={id=" + group.getId() +
                ", name=" + group.getName() +
                ", graduationYear=" + group.getGraduationYear() + "}}";
    }
}
