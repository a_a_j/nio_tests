package dao;

import model.Group;
import model.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentImpl implements StudentDao {

    Map<Integer, Student> students = new HashMap();

    public StudentImpl() {
        Group group = new Group(1, "IT Group", 2007);
        students.put(1, new Student(1, "Zharikov Alexander Arturovich", "Moscow", group));
        students.put(2, new Student(2, "Petrov Ivan Ivanovich", "Moscow", group));
    }

    public void addStudent(Student student) {
        students.put(student.getId(), student);
    }

    public void deleteStudent(int id) {
        students.remove(id);
    }

    public Student getStudent(int id) {
        return students.get(id);
    }

    public List<Student> getAllStudents() {
        return new ArrayList(students.values());
    }
}
