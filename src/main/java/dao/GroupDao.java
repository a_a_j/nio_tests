package dao;

import model.Group;

import java.util.List;

public interface GroupDao {
    void addGroup(Group group);

    void deleteGroup(int id);

    void updateGroup(Integer id, String name, Integer year);

    Group getGroup(int id);

    List<Group> getAllGroups();
}
