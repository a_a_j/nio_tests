import model.Student;
import dao.StudentImpl;
import service.StudentService;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        StudentService studentService = new StudentService(new StudentImpl());

        System.out.println("[WRITE]");
        Student studentToFile = studentService.getStudentById(1);
        System.out.println(studentToFile.toString());
        studentService.save(studentToFile);
        studentToFile = studentService.getStudentById(2);
        System.out.println(studentToFile.toString());
        studentService.save(studentToFile);

        System.out.println("[READ]");
        List<Student> studentList = studentService.getStudents(1);
        for (Student s: studentList) {
            System.out.println(s.toString());
        }

    }
}
